/*********************************************************************
  UDF that changes the time step value for a time-dependent solution
 **********************************************************************/
 #include "udf.h"
 #define Num_p  1000 //耦合边界上单元数目
 #define Num_t 40000 //实际计算的时间步数目
 real x_0[Num_p],y_0[Num_p];
 real x_1[Num_p],y_1[Num_p];
 real pres_0[Num_p];
 real pres_1[Num_p]; 
 real E_wk[Num_t];
 real dtime[Num_t];
 
 int i_n=0;//实际运行的时间步的数量，与当前时间步序号N_TIME不同。
 
 //确定当前时间步的步长。该子程序在时间步迭代之前执行。
 DEFINE_DELTAT(mydeltat,d)
 {
    real time_step;


    //int zone_ID = 6;  
    //real t_1 = CURRENT_TIME; //当前时间
    //real t_0 = PREVIOUS_TIME;//上一个时间步的时间    
    //
    //i_n=i_n+1; //实际运行的时间步的数量 
    //
    //if (i_n>1)
    //{
    //  ttt = Lookup_Thread(d,zone_ID);//查找耦合边界  
    //  fp1=fopen("test-x1.txt","a");
    //  fprintf(fp1,"\n\n\n\n%5d\n",i_n);
    //  begin_f_loop(f,ttt)
    //  { 
    //    F_AREA(A,f,ttt);
    //    F_CENTROID(x,f,ttt);
    //    x_1[f]=x[0];//当前时间步的x坐标，应该是更新完网格的坐标
    //    y_1[f]=x[1];//当前时间步的y坐标，应该是更新完网格的坐标
    //    
    //    v_x=(x_1[f]-x_0[f])/(t_1-t_0);//边界单元中心的x方向变形速度
    //    v_y=(y_1[f]-y_0[f])/(t_1-t_0);//边界单元中心的y方向变形速度
    //    //E_wk[i_n]+=f_x[f]*v_x+f_y[f]*v_y; //边界单元中心力对单元做功的功率
    //    //E_wk_sum=f_x[f]*v_x+f_y[f]*v_y; //边界单元中心力对单元做功的功率
    //    //fprintf(fp1,"%15.8f%15.8f%15.8f\n",A[0],A[1],A[2]);
    //    //fprintf(fp1,"%15.8f%15.8f%15.8f\n",A[0]/NV_MAG(A),A[1]/NV_MAG(A),A[2]);
    //    //fprintf(fp1,"%3d%15.8f%15.8f%15.8f%15.8f\n",f,x[0],x[1],f_x,f_y);
    //    //NV_MAG(A) 为返回边界单元向量的摸
    //    //A[0] 面积向量在x方向分量 //A[1] 面积向量在y方向分量  //A[2] 面积向量在z方向分量
    //    fprintf(fp1,"%15.8f%15.8f%15.8f%15.8f\n",x_1[f],y_1[f],v_x,v_y);
    //  }
    //  end_f_loop(f,ttt)
    //  fclose(fp1);
    //}
    time_step=1E-4;//默认的时间步长是1e-4
    if (i_n>1000)//先以固定时间步长运行1000步，就是0.1 sec，然后再执行自适应时间步长
    {
    }
  
  
  
  
  
  
  return time_step;
 } 
 
 //确定时间步结束的时候，耦合边界的功率。
 DEFINE_EXECUTE_AT_END(execute_at_end)
 {
    Domain *d;
    Thread *ttt;
    face_t f;
    FILE *fp2;//文件读写操作号。
    
    real x[ND_ND];
    real A[ND_ND];
    real A_0;
    real n_x,n_y;
    real pres;
    real v_x,v_y;
    real f_x,f_y;    
    real E_wk_sum=0.0;
    real f_x_sum=0.0;
    real f_y_sum=0.0;
    real v_x_sum=0.0;
    real v_y_sum=0.0;
    real p_0=0.00572344*1e6;//壁板下方的静压，方向沿+y

    
    int zone_ID = 6;  
    real t_1 = CURRENT_TIME; //当前时间
    real t_0 = PREVIOUS_TIME;//上一个时间步的时间
    real dt=t_1-t_0;
    
    i_n=i_n+1; //实际运行的时间步的数量
    dtime[i_n]=dt;

    
    
    d = Get_Domain(1);//查找domain
    ttt = Lookup_Thread(d,zone_ID);//查找耦合边界
    
    
    //if (i_n>1) //在第n+1个时间步，x_1将被abaqus更新，而x_0则是第n个时间步的x_1的值
    //{
    //NV_V(x_0 ,=, x_1);//将当前时间步的x坐标保存
    //NV_V(y_0 ,=, y_1);//将当前时间步的y坐标保存    
    //} 
    fp2=fopen("test-x0.txt","a");
    //fprintf(fp2,"\n\n\n\n%5d\n",i_n);
    //fprintf(fp2,"%15.8f\n",t_1);
    begin_f_loop(f,ttt)//在耦合边界上遍历查找单元
    { 
      ///////// 确定单元中心坐标
      if (i_n>1) //n+1时刻，x_1要更新为存储n时刻的坐标，而x_0也更新为n-1时刻的坐标
      {
       x_0[f]=x_1[f] ;// n-1时刻，边界单元中心的坐标
       y_0[f]=y_1[f] ;// n-1时刻，边界单元中心的坐标
      }      
      F_CENTROID(x,f,ttt);//时间步结束时边界单元的中心
      x_1[f]=x[0];//n时刻，边界单元中心的x坐标
      y_1[f]=x[1];//n时刻，边界单元中心的y坐标
      if (i_n==1) //在第1个时间步，强制认为x_0和x_1是一样的。
      {
       x_0[f]=x_1[f] ;
       y_0[f]=y_1[f] ;
      }
      
      ///// 确定单元法向量
      F_AREA(A,f,ttt);  //边界单元面积向量
      n_x=A[0]/NV_MAG(A);//边界单元法向量x分量
      n_y=A[1]/NV_MAG(A);//边界单元法向量y分量
      A_0=NV_MAG(A)/1000.0;//边界单元法向量的模，即面积
                           //note:这里除以1000的原因如下。默认2D模型垂直平面方向的尺度是1，单位由求解器确定。
                           //Fluent的单位是m-kg-t，因而默认是厚度1m；
                           //Abaqus的单位是mm-T-s，默认平面应力单元的厚度为1mm。
                           //需要协调一下。
      A_0=A_0*1000.0; //这里又乘了1000，是因为功率数量级太小：4e-3 N*m/s。怕运算失真。乘以1000.
      
      /////确定单元受的压力
      if (i_n>1)
      {
      pres_0[f]=pres_1[f];// n 时刻，边界单元上的静压力
      }
      
      pres_1[f]=F_P(f,ttt); //n+1时刻，边界单元上的静压力
      
      if (i_n==1) //在第1个时间步，强制认为pres_0和pres_1是一样的。
      {
        pres_0[f]=pres_1[f];
      }
      
      
      
      //f_x=pres_0[f]*A_0*n_x+p_0*A_0*(-n_x);//n时刻，边界单元合力x分量，p_0是静载荷
      //f_y=pres_0[f]*A_0*n_y+p_0*A_0*(-n_y);//n时刻，边界单元合力y分量，p_0是静载荷
      f_x=pres_0[f]*A_0*n_x+p_0*A_0*(0.0);//n时刻，边界单元合力x分量，p_0是静载荷
      f_y=pres_0[f]*A_0*n_y+p_0*A_0*(1.0);//n时刻，边界单元合力y分量，p_0是静载荷

      //printf("%15.8f%15.8f%15.8f\n",pres,f_x,f_y);
      
      if (i_n==1)
      {
      v_x=(x_1[f]-x_0[f])/(dtime[i_n]);
      v_y=(y_1[f]-y_0[f])/(dtime[i_n]);
      }
      v_x=(x_1[f]-x_0[f])/(dtime[i_n-1]);//n时刻，边界单元中心的x方向变形速度
      v_y=(y_1[f]-y_0[f])/(dtime[i_n-1]);//n时刻，边界单元中心的y方向变形速度 

      ///
      E_wk_sum+=f_x*v_x+f_y*v_y;//n时刻，耦合边界的功率
      f_x_sum+=f_x;
      f_y_sum+=f_y;
      v_x_sum+=v_x;
      v_y_sum+=v_y;
      //if(t_1>0.0001)      
      //fprintf(fp2,"%15.8f%15.8f%15.8f%15.8f\n",x_1[f],y_1[f],v_x,v_y);
      
      //printf("%d\n",i_n);
      if (f==20)
      printf("%18.5f%18.5f\n",A_0,pres_1[f]);
    }
    end_f_loop(f,ttt)

    
    //fprintf(fp2,"%15.8f%15.8f\n",x_0[1],x_1[1]);
    if(i_n==1){fprintf(fp2,"Index   time    WorkRate   f_x    f_y     v_x     v_y\n");}
    fprintf(fp2,"%d%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",i_n,t_0,E_wk_sum,f_x_sum,f_y_sum,v_x_sum,v_y_sum);
    fclose(fp2);
 }