/*********************************************************************
  UDF that changes the tijme step value for a tijme-dependent solutijon
 **********************************************************************/
 #include "udf.h"
 #define Num_p  1000 //耦合边界上单元数目
 #define Num_t 40000 //实际计算的时间步数目
 real x_0[Num_p],y_0[Num_p];
 real x_1[Num_p],y_1[Num_p];
 real pres_0[Num_p];
 real pres_1[Num_p]; 
 real E_wk_t[Num_t];
 real f_x_t[Num_t],f_y_t[Num_t];
 real v_x_t[Num_t],v_y_t[Num_t];
 real dtijme[Num_t],tijme[Num_t];
 
 int i_n=0;//实际运行的时间步的数量，与当前时间步序号N_tijME不同。
 
 //确定当前时间步的步长。该子程序在时间步迭代之前执行。
DEFINE_DELTAT(mydeltat,d)
{
    FILE *fp3;
    real tijme_step;
    real Indicate[Num_t];
    real MaxVal=0;
    real x1,x2,x3,x4,x5,x6,x7;
    real y1,y2,y3,y4,y5,y6,y7;
    float a1=0.0,a2=0.0,a3=0.0,a4=0.0,a5=0.0;
    float tjj,tij;
    float xx[6],yy[6];
    int i,j,k;
    float M1[6][7];
    float M[6][7]=
    {
    {0.0,   0.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    };
    

    NV_V( Indicate ,=, E_wk_t);   //自适应参考准则：E_wk变化
    //NV_V( Indicate ,=, f_y_t);  //自适应参考准则：f_y变化
    //NV_V( Indicate ,=, v_y_t);  //自适应参考准则：v_y变化
    
    tijme_step=1E-4;//默认的时间步长是1e-4
    //if (i_n+1>10)//先以固定时间步长运行4000步，就是0.4 sec，然后再执行自适应时间步长
    //{              //DEFINE_DELTAT中计数器比DEFINE_EXECUTE_AT_END多1个。前者在迭代之前执行，后者在迭代结束后执行。
    //  //而E_wk_t、v_x_t、v_y_t、f_x_t、f_y_t的计数器又比DEFINE_EXECUTE_AT_END少1个。
    //  x1=tijme[i_n-1];
    //  x2=tijme[i_n-2];
    //  x3=tijme[i_n-3];
    //  x4=tijme[i_n-4];
    //  x5=tijme[i_n-5];
    //  y1=Indicate[i_n-1];
    //  y2=Indicate[i_n-2];
    //  y3=Indicate[i_n-3];
    //  y4=Indicate[i_n-4];
    //  y5=Indicate[i_n-5];
    //  
    //  //下面用多项式拟合预测(x6,y6)，(x7,y7)。多项式方法适合预测三角函数的一小段，小于1/4周期。
    //  //x6是i_n对应的时间，已知。
    //  //x7是i_n+1对应的时间，正式要在DEFINE_DELTAT中确定的。
    //  x6=tijme[i_n];
    //  x7=x6+tijme_step;
    //  Predict(y7,x7,x5,x4,x3,x2,x1,y5,y4,y3,y2,y1);
    //  //for (int i=i_n;i>i_n-100;i--)
    //  //{
    //  //  if (MaxVal<fabs(Indicate[i]))
    //  //  MaxVal=fabs(Indicate[i];
    //  //}
    //  //if (fabs(y7)<MaxVal/4.0)// 如果小于1/4最值，粗化时间
    //  //tijme_step=tijme_step*2.0;
    //}
    
    //  c语言中数组排列如下, int a[3][4];
    //  a[0][0], a[0][1], a[0][2], a[0][3]
    //  a[1][0], a[1][1], a[1][2], a[1][3]
    //  a[2][0], a[2][1], a[2][2], a[2][3]
    //  参考 http://see.xidian.edu.cn/cpp/html/51.html
    // 用多项式拟合，要确定多项式系数a1、a2、a3、a4、a5
    // a1+a2*x+a3*x^2+a4*x^3+a5*x^4=y
    // [ 1, x1, x1^2, x1^3, x1^4][a1]=[y1]
    // [ 1, x2, x2^2, x2^3, x2^4][a2]=[y2]
    // [ 1, x3, x3^2, x3^3, x3^4][a3]=[y3]
    // [ 1, x4, x4^2, x4^3, x4^4][a4]=[y4]
    // [ 1, x5, x5^2, x5^3, x5^4][a5]=[y5]
    

    

    x1=1.0;
    x2=2.0;
    x3=3.0;
    x4=4.0;
    x5=5.0;
    x6=6.0;
    y1=0.309016994;
    y2=0.587785252;
    y3=0.809016994;
    y4=0.951056516;
    y5=1.0;
    y6=0.0;
    
    
    // 矩阵赋值
    xx[1]=x1;
    xx[2]=x2;
    xx[3]=x3;
    xx[4]=x4;
    xx[5]=x5;
    
    M[1][6]=y1;
    M[2][6]=y2;
    M[3][6]=y3;
    M[4][6]=y4;
    M[5][6]=y5;
    
    for (i=1;i<=5;i++)
    {
      for(j=2;j<=5;j++)
        {
        M[i][j]=pow(xx[i],j-1);
        }
    }
   

   //测试 保存原矩阵
    for (i=1;i<=5;i++)
    {
      for(j=1;j<=5;j++)
        {
        M1[i][j]=M[i][j];
        }
    }

    //测试矩阵输入是否正确
    fp3=fopen("test-M_0.txt","w");
    //fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",x1,x2,x3,x4,x5);
    for (i=1;i<=5;i++)
    {
        fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][1],M[i][2],M[i][3],M[i][4],M[i][5],M[i][6]);
    }
    fclose(fp3);    
    
    // 下三角置零
    for(j=1;j<=4;j++)
    {
       for(i=j+1;i<=5;i++)
       {  tjj  =M[j][j];
          tij  =M[i][j];
          for (k=1;k<=6;k++)
          {
            M[i][k]=M[i][k]-M[j][k]*tij/tjj;
          }
          
       }
    }
    
    //测试下三角置零是否正确
    fp3=fopen("test-M_1.txt","w");
    for (i=1;i<=5;i++)
    {
        fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][1],M[i][2],M[i][3],M[i][4],M[i][5],M[i][6]);
    }
    fclose(fp3);
    
    //计算多项式系数
    a5=(M[5][6])                                            /M[5][5];
    a4=(M[4][6]-a5*M[4][5])                                 /M[4][4];
    a3=(M[3][6]-a5*M[3][5]-a4*M[3][4])                      /M[3][3];
    a2=(M[2][6]-a5*M[2][5]-a4*M[2][4]-a3*M[2][3])           /M[2][2];
    a1=(M[1][6]-a5*M[1][5]-a4*M[1][4]-a3*M[1][3]-a2*M[1][2])/M[1][1];
    
    //测试多项式系数是否正确
    fp3=fopen("test-M_2.txt","w");
    fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",a1,a2,a3,a4,a5);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x1,a1*M1[1][1]+a2*M1[1][2]+a3*M1[1][3]+a4*M1[1][4]+a5*M1[1][5],y1);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x2,a1*M1[2][1]+a2*M1[2][2]+a3*M1[2][3]+a4*M1[2][4]+a5*M1[2][5],y2);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x3,a1*M1[3][1]+a2*M1[3][2]+a3*M1[3][3]+a4*M1[3][4]+a5*M1[3][5],y3);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x4,a1*M1[4][1]+a2*M1[4][2]+a3*M1[4][3]+a4*M1[4][4]+a5*M1[4][5],y4);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x5,a1*M1[5][1]+a2*M1[5][2]+a3*M1[5][3]+a4*M1[5][4]+a5*M1[5][5],y5);
    fclose(fp3);    
    
    y6= a1  +   a2*x6  +  a3*pow(x6,2)  +  a4*pow(x6,3)  +  a5*pow(x6,4);  
  
  return tijme_step;
} 
 

 //确定时间步结束的时候，耦合边界的功率。
DEFINE_EXECUTE_AT_END(execute_at_end)
{
    Domain *d;
    Thread *ttt;
    face_t f;
    FILE *fp2;//文件读写操作号。
    
    real x[ND_ND];
    real A[ND_ND];
    real A_0;
    real n_x,n_y;
    real pres;
    real v_x;
    real v_y;
    real f_x;
    real f_y;    
    real E_wk_sum=0.0;
    real f_x_sum=0.0;
    real f_y_sum=0.0;
    real v_x_sum=0.0;
    real v_y_sum=0.0;
    real p_0=0.00572344*1e6;//壁板下方的静压，方向沿+y

    
    int zone_ID = 6;  
    real t_1 = CURRENT_TIME; //当前时间
    real t_0 = PREVIOUS_TIME;//上一个时间步的时间
    real dt=t_1-t_0;
    
    i_n=i_n+1; //实际运行的时间步的数量
    dtijme[i_n]=dt;
    //printf("%d%18.5f%18.5f\n",i_n,dtijme[i_n],dt);
    if (i_n==1)
    {
    fp2=fopen("test-x0.txt","w");
    fclose(fp2);
    }

    
    
    d = Get_Domain(1);//查找domain
    ttt = Lookup_Thread(d,zone_ID);//查找耦合边界
    
    
    //if (i_n>1) //在第n+1个时间步，x_1将被abaqus更新，而x_0则是第n个时间步的x_1的值
    //{
    //NV_V(x_0 ,=, x_1);//将当前时间步的x坐标保存
    //NV_V(y_0 ,=, y_1);//将当前时间步的y坐标保存    
    //} 
    fp2=fopen("test-x0.txt","a");
    //fprintf(fp2,"\n\n\n\n%5d\n",i_n);
    //fprintf(fp2,"%15.8f\n",t_1);
    begin_f_loop(f,ttt)//在耦合边界上遍历查找单元
    { 
      ///////// 确定单元中心坐标
      if (i_n>1) //n+1时刻，x_1要更新为存储n时刻的坐标，而x_0也更新为n-1时刻的坐标
      {
       x_0[f]=x_1[f] ;// n-1时刻，边界单元中心的坐标
       y_0[f]=y_1[f] ;// n-1时刻，边界单元中心的坐标
      }      
      F_CENTROID(x,f,ttt);//时间步结束时边界单元的中心
      x_1[f]=x[0];//n时刻，边界单元中心的x坐标
      y_1[f]=x[1];//n时刻，边界单元中心的y坐标
      if (i_n==1) //在第1个时间步，强制认为x_0和x_1是一样的。
      {
       x_0[f]=x_1[f] ;
       y_0[f]=y_1[f] ;
      }
      
      ///// 确定单元法向量
      F_AREA(A,f,ttt);  //边界单元面积向量
      n_x=A[0]/NV_MAG(A);//边界单元法向量x分量
      n_y=A[1]/NV_MAG(A);//边界单元法向量y分量
      A_0=NV_MAG(A)/1000.0;//边界单元法向量的模，即面积
                           //note:这里除以1000的原因如下。默认2D模型垂直平面方向的尺度是1，单位由求解器确定。
                           //Fluent的单位是m-kg-t，因而默认是厚度1m；
                           //Abaqus的单位是mm-T-s，默认平面应力单元的厚度为1mm。
                           //需要协调一下。
      A_0=A_0*1000.0; //这里又乘了1000，是因为功率数量级太小：4e-3 N*m/s。怕运算失真。乘以1000.
      
      /////确定单元受的压力
      if (i_n>1)
      {
      pres_0[f]=pres_1[f];// n 时刻，边界单元上的静压力
      }
      
      pres_1[f]=F_P(f,ttt); //n+1时刻，边界单元上的静压力
      
      if (i_n==1) //在第1个时间步，强制认为pres_0和pres_1是一样的。
      {
        pres_0[f]=pres_1[f];
      }
      
      
      
      //f_x=pres_0[f]*A_0*n_x+p_0*A_0*(-n_x);//n时刻，边界单元合力x分量，p_0是静载荷
      //f_y=pres_0[f]*A_0*n_y+p_0*A_0*(-n_y);//n时刻，边界单元合力y分量，p_0是静载荷
      f_x=pres_0[f]*A_0*n_x+p_0*A_0*(0.0);//n时刻，边界单元合力x分量，p_0是静载荷
      f_y=pres_0[f]*A_0*n_y+p_0*A_0*(1.0);//n时刻，边界单元合力y分量，p_0是静载荷，沿+y轴 

      //printf("%15.8f%15.8f%15.8f\n",pres,f_x,f_y);
      
      if (i_n==1)//在第1个时间步，强制认为速度为零
      {
      v_x=(x_1[f]-x_0[f])/(dtijme[i_n]);
      v_y=(y_1[f]-y_0[f])/(dtijme[i_n]);
      //printf("%d%15.8f%15.8f\n",i_n,dtijme[i_n],v_y);
      }
      else
      {
      v_x=(x_1[f]-x_0[f])/(dtijme[i_n-1]);//n时刻，边界单元中心的x方向变形速度
      v_y=(y_1[f]-y_0[f])/(dtijme[i_n-1]);//n时刻，边界单元中心的y方向变形速度 
      }
      
      ///
      E_wk_sum =E_wk_sum+f_x*v_x+f_y*v_y;//n时刻，耦合边界的功率
      f_x_sum  =f_x_sum+f_x;
      f_y_sum  =f_y_sum+f_y;
      v_x_sum  =v_x_sum+v_x;
      v_y_sum  =v_y_sum+v_y;
      //if(t_1>0.0001)      
      //fprintf(fp2,"%15.8f%15.8f%15.8f%15.8f\n",x_1[f],y_1[f],v_x,v_y);
      
      //printf("%d\n",i_n);
      //if (i_n==1)
      //printf("%d%18.5f%18.5f\n",i_n,v_x_sum,v_y_sum);
    }
    end_f_loop(f,ttt)
    
    //printf("%d%18.5f%18.5f\n",i_n,dtijme[i_n],dt);

    
    //fprintf(fp2,"%15.8f%15.8f\n",x_0[1],x_1[1]);
    if(i_n==1)
    {
    fprintf(fp2,"Index   tijme    WorkRate   f_x    f_y     v_x     v_y\n");
    }
    else
    {
    fprintf(fp2,"%d%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",i_n-1,t_0,E_wk_sum,f_x_sum,f_y_sum,v_x_sum,v_y_sum);
    E_wk_t[i_n-1]=E_wk_sum;
    v_x_t [i_n-1]=v_x_sum;
    v_y_t [i_n-1]=v_y_sum;
    f_x_t [i_n-1]=f_x_sum;
    f_y_t [i_n-1]=f_y_sum;
    tijme  [i_n-1]=t_0;
    }
    fclose(fp2);
}

void Predict(float y6, float x6, float x5, float x4,float x3,float x2,float x1,float y5,float y4,float y3,float y2,float y1)
{  // a1+a2*x+a3*x^2+a4*x^3+a5*x^4=y
    //
    // [ 1, x1, x1^2, x1^3, x1^4][a1]=[y1]
    // [ 1, x2, x2^2, x2^3, x2^4][a2]=[y2]
    // [ 1, x3, x3^2, x3^3, x3^4][a3]=[y3]
    // [ 1, x4, x4^2, x4^3, x4^4][a4]=[y4]
    // [ 1, x5, x5^2, x5^3, x5^4][a5]=[y5]
    
    //  c语言中数组排列如下, int a[3][4];
    //  a[0][0], a[0][1], a[0][2], a[0][3]
    //  a[1][0], a[1][1], a[1][2], a[1][3]
    //  a[2][0], a[2][1], a[2][2], a[2][3]
    //  参考 http://see.xidian.edu.cn/cpp/html/51.html
    
    FILE *fp3;
    //real x1,x2,x3,x4,x5,x6;
    //real y1,y2,y3,y4,y5,y6;
    float a1=0.0,a2=0.0,a3=0.0,a4=0.0,a5=0.0;
    float tjj,tij;
    float xx[6],yy[6];
    int i,j;
    float M[6][7]=
    {
    {0.0,   0.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    };
    
    xx[1]=x1;
    xx[2]=x2;
    xx[3]=x3;
    xx[4]=x4;
    xx[5]=x5;
    
    M[1][6]=y1;
    M[2][6]=y2;
    M[3][6]=y3;
    M[4][6]=y4;
    M[5][6]=y5;
    
    for (i=1;i<=5;i++)
    {
      for(j=2;j<=5;j++)
        {
        M[i][j]=pow(xx[i],j-1);
        }
    }
   
    
    //M[6][8]=0.0;
    //M[0]=0.0;

    //测试
    fp3=fopen("test-M_0.txt","w");
    fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",x1,x2,x3,x4,x5);
    for (i=0;i<=5;i++)
    {
        fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][0],M[i][1],M[i][2],M[i][3],M[i][4],M[i][5],M[i][6]);
    }
    fclose(fp3);    
    
    // 下三角置零
    for(j=1;j<=4;j++)
    {
       for(i=j+1;i<=5;i++)
       {  tjj=M[i-1][j];
          tij  =M[i][j];
          //M[i]=M[i]-M[i-1]*tij/tjj;
          NV_V_VS(M[i]  ,=,  M[i]  ,-,  M[i-1]  ,*,  tij/tjj);
       }
    }
    
    //测试
    fp3=fopen("test-M_1.txt","w");
    for (i=1;i<=5;i++)
    {
        fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][1],M[i][2],M[i][3],M[i][4],M[i][5]);
    }
    fclose(fp3);
    
    a5=(M[5][6])                                            /M[5][5];
    a4=(M[4][6]-a5*M[4][5])                                 /M[4][4];
    a3=(M[3][6]-a5*M[3][5]-a4*M[3][4])                      /M[3][3];
    a2=(M[2][6]-a5*M[2][5]-a4*M[2][4]-a3*M[2][3])           /M[2][2];
    a1=(M[1][6]-a5*M[1][5]-a4*M[1][4]-a3*M[1][3]-a2*M[1][2])/M[1][1];
    
    //测试
    fp3=fopen("test-M_2.txt","w");
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x1,a1*M[1][1]+a2*M[1][2]+a3*M[1][3]+a4*M[1][4]+a5*M[1][5],y1);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x2,a1*M[2][1]+a2*M[2][2]+a3*M[2][3]+a4*M[2][4]+a5*M[2][5],y2);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x3,a1*M[3][1]+a2*M[3][2]+a3*M[3][3]+a4*M[3][4]+a5*M[3][5],y3);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x4,a1*M[4][1]+a2*M[4][2]+a3*M[4][3]+a4*M[4][4]+a5*M[4][5],y4);
    fprintf(fp3,"%15.8f%15.8f%15.8f\n",x5,a1*M[5][1]+a2*M[5][2]+a3*M[5][3]+a4*M[5][4]+a5*M[5][5],y5);
    fclose(fp3);    
    
    y6= a1  +   a2*x6  +  a3*pow(x6,2)  +  a4*pow(x6,3)  +  a5*pow(x6,4);
};