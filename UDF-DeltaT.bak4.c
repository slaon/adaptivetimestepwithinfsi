/*********************************************************************
  UDF that changes the time step value for a time-dependent solution
 **********************************************************************/
 #include "udf.h"
 //real x_0[800],y_0[800];
 //real x_1[800],y_1[800];
 //int i_n=0;
 
 DEFINE_DELTAT(mydeltat,d)
 {
  real time_step;
  //face_t f;
  //real A[ND_ND];
  //real x[ND_ND];
  //real vel[ND_ND];
  //Thread *ttt;
  //
  //real time_step;
  //real temp;
  //real t_1 = CURRENT_TIME; //当前时间
  //real t_0 = PREVIOUS_TIME;//上一个时间步的时间
  //real pres;
  //real f_x,f_y;
  //real n_x,n_y;
  //real v_x,v_y;
  //real A_0;
  //real E_w;
  //real pi=M_PI;
  ////real freq=100;
  ////real t_amp=5e-5;
  ////real t_avg=1e-4;
  //FILE *fp;
  
  //int i,i_step;
  //int zone_ID = 6;  
  ////NV_S(vel,=,0);
  //
  //i_step=N_TIME;
  //i_n=i_n+1;
  //
  //ttt = Lookup_Thread(d,zone_ID);
  //
  //fp=fopen("test.txt","a");
  ////fprintf(fp,"%15.8f\n",t_1);
  //begin_f_loop(f,ttt)
  //{
  //  F_AREA(A,f,ttt);
  //  F_CENTROID(x,f,ttt);
  //  x_1[f]=x[0];//当前时间步的x坐标
  //  y_1[f]=x[1];//当前时间步的y坐标
  //  
  //  n_x=A[0]/NV_MAG(A);//边界单元法向量x分量
  //  n_y=A[1]/NV_MAG(A);//边界单元法向量y分量
  //  pres=F_P(f,ttt); //边界单元上的静压力
  //  A_0=NV_MAG(A);//边界单元法向量的模，即面积
  //  f_x=pres*A_0*n_x;//边界单元受力x分量
  //  f_y=pres*A_0*n_y;//边界单元受力y分量
  //  v_x=(x_1[f]-x_0[f])/(t_1-t_0);//边界单元中心的x方向变形速度
  //  v_y=(y_1[f]-y_0[f])/(t_1-t_0);//边界单元中心的y方向变形速度
  //  
  //  //fprintf(fp,"%15.8f%15.8f%15.8f\n",A[0],A[1],A[2]);
  //  //fprintf(fp,"%15.8f%15.8f%15.8f\n",A[0]/NV_MAG(A),A[1]/NV_MAG(A),A[2]);
  //  //fprintf(fp,"%3d%15.8f%15.8f%15.8f%15.8f\n",f,x[0],x[1],f_x,f_y);
  //  //NV_MAG(A) 为返回边界单元向量的摸
  //  //A[0] 面积向量在x方向分量 //A[1] 面积向量在y方向分量  //A[2] 面积向量在z方向分量
  //  //printf("%15f%15f%15f\n",A[0],A[1],A[2]);
  //}
  //end_f_loop(f,ttt)
  ////fprintf(fp,"%15.8f%15.8f\n",x_0[1],x_1[1]);
  //fprintf(fp,"%d\n",i_n);
  //NV_V(x_0 ,=, x_1);//将当前时间步的x坐标保存
  //NV_V(y_0 ,=, y_1);//将当前时间步的y坐标保存
  //fclose(fp);
  //
  ////fp=fopen("test.txt","w");
  ////  for(i=0;i<i_step;i++)
  ////  {
  ////  fscanf(fp,"%f",&temp);
  ////  //printf("%d\n",i);
  ////  }
  ////  fscanf(fp,"%f",&time_step);
  ////  printf("%d\n",i);
  ////  printf("%f\n",time_step);
  ////fclose(fp);
  ////printf("%d\n",i_step);
  //
  //
  ////time_step=t_avg+sin(2*pi*f*t_1)*t_amp;
  time_step=5E-5;
  return time_step;
  

 } 
 
 
//  DEFINE_CG_MOTION(pod,dt,vel,omega,time,dtime)
// {
//  face_t f;
//  Thread *ttt;
//  Domain *d;          /* domain is declared as a variable  */
//  d = Get_Domain(2);   
//  int zone_ID = 6; 
//
//
//  NV_S(vel,=,0);
//  NV_S(omega,=,0);
//  ttt = Lookup_Thread(d,zone_ID);
//  begin_f_loop(f,ttt)
//  {
//  F_CENTROID(x,f,ttt);
//  vel[1] = sin(2.0*M_PI*1.0*x[0])*time*1.0e3;
//  }
//  end_f_loop(f,ttt)
// } 