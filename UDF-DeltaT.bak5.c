/*********************************************************************
   UDF that changes the time step value for a time-dependent solution
 **********************************************************************/
 #include "udf.h"
 
 DEFINE_DELTAT(mydeltat,d)
 {
   real time_step;
   real flow_time = CURRENT_TIME;
   if (flow_time < 0.5)
       time_step = 0.001;
   else
       time_step = 0.001;
   return time_step;
 }