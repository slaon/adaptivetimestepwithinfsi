/*********************************************************************
   UDF that changes the time step value for a time-dependent solution
 **********************************************************************/
 #include "udf.h"
 
 DEFINE_DELTAT(mydeltat,d)
 {
   real time_step;
   real temp;
   real flow_time = CURRENT_TIME;
   real pi=3.1415926;
   real f=100;
   real t_amp=5e-5;
   real t_avg=1e-4;
   FILE *fp;
   int i,i_step;
   i_step=N_TIME;
   
   //fp=fopen("test.txt","r");
   //  for(i=3;i<i_step;i++)
   //  {
   //  fscanf(fp,"%f",&temp);
   //  printf("%d\n",i);
   //  }
   //  fscanf(fp,"%f",&time_step);
   //fclose(fp);
   //printf("%d\n",i_step);
   
   
   //time_step=t_avg+sin(2*pi*f*flow_time)*t_amp;
   time_step=0.0001;
   return time_step;
 } 