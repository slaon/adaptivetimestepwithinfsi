/*********************************************************************
  UDF that changes the time step value for a time-dependent solutijon
 **********************************************************************/
 #include "udf.h"
 #include <string.h>
 #define Num_p  1000 //耦合边界上单元数目
 #define Num_t 40000 //实际计算的时间步数目
 double p_0=0.00572344*1e6;//壁板下方的静压，方向沿+y 
 double x_0[Num_p]={0.0},y_0[Num_p]={0.0};
 double x_1[Num_p]={0.0},y_1[Num_p]={0.0};
 double pres_0[Num_p]={0.0};
 double pres_1[Num_p]={0.0}; 
 double E_wk_t[Num_t]={0.0};
 double f_x_t[Num_t]={0.0},f_y_t[Num_t]={0.0};
 double v_x_t[Num_t]={0.0},v_y_t[Num_t]={0.0};
 double dtime[Num_t]={0.0},time[Num_t] ={0.0};
 double Indicate[Num_t]={0.0};
 double dt1=0.0001, dt2=0.0002;
 
 int i_n=0;//实际运行的时间步的数量，与当前时间步序号N_timE不同。
 
 //确定当前时间步的步长。该子程序在时间步迭代之前执行。
DEFINE_DELTAT(mydeltat,d)
{
    FILE *fp3;
    double time_step;
    
    int  Num_1=2000;
    int  Num_2=Num_1;
    int  ii;
    double MaxVal=0.0;
    double x1,x2,x3,x4,x5,x6,x7;
    double xx1,xx2,xx3,xx4,xx5,xx6,xx7;
    double y1,y2,y3,y4,y5,y6,y7;
    double Sigma=0.0, Tolerent=0.0;
    double a1=0.0,a2=0.0,a3=0.0,a4=0.0,a5=0.0;
    double tjj,tij;
    double xx[6],yy[6];
    double  xxxx;
    int i,j,k;
    double M1[6][7];
    double M[6][7]=
    {
    {0.0,   0.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    {0.0,   1.0,   0.0,0.0,0.0,0.0,0.0},
    };
    

    


    
    if (i_n==2)
    {
        fp3=fopen("test-work.txt","w");
        fprintf(fp3,"index   time    indicate\n");
        fclose(fp3);   
        fp3=fopen("test-M_0.txt","w");    
        fclose(fp3);     
        fp3=fopen("test-M_1.txt","w");    
        fclose(fp3);       
        fp3=fopen("test-M_2.txt","w");    
        fclose(fp3);   
        fp3=fopen("test-y.txt","w");
        fclose(fp3);   
        fp3=fopen("test-Sigma.txt","w");
        fclose(fp3);           
    }
    
    
    
    
   
    if (i_n+1>Num_1)//先以固定时间步长运行2000步，就是0.2 sec，然后再执行自适应时间步长
    {         
        ////////////////////
        /////  如果前5步的时间步长出现跳跃：细化 或者 粗化
        /////  那么fluent计算的载荷在时间步长突跳的那一时间步会出现载荷突跳，
        /////  从而导致该时间步的功率出现异常。但后续时间步不受影响。
        /////  因此，如果出现时间步长突跳，则保证其后的4步时间步长与突跳后的步长保持一致
        /////  这样，在根据n-3、n-2、n-1个间步预测第n、n+1步的功率时，不会出现异常
        Sigma=dtime[i_n]+dtime[i_n-1]+dtime[i_n-2]+dtime[i_n-3]+dtime[i_n-4];
        if ( Sigma>5.5*dt1 && Sigma<4.5*dt2) ///// 前5步没有突跳的话，其和要么为4*dt1，要么为4*dt2；否则在二者之间。
        {
            time_step=dtime[i_n];
            fp3=fopen("test-Sigma.txt","a");
                fprintf(fp3,"%20.8f%20.8f%20.8f%20.8f%20.8f%20.8f\n",dtime[i_n],dtime[i_n-1],dtime[i_n-2],dtime[i_n-3],dtime[i_n-4]);
                fprintf(fp3,"%8d%20.8f%20.8f%20.8f\n",i_n+1,5*dt1,Sigma,5*dt2);
            fclose(fp3);                
            goto Label_Return;
        }
        
        
        //////////////////////////////////
        ////  确定参考准则
        ////for (i=i_n-5;i<=i_n-1;i++)
        ////{
        //// //Indicate[i]=E_wk_t[i];//自适应参考准则：E_wk变化
        //// Indicate[i]=f_y_t[i];//自适应参考准则：f_y变化
        //// //Indicate[i]=v_y_t[i];//自适应参考准则：v_y变化
        ////}
        
        ii=1;
        if      (ii == 1)
        {
            for (i=1;i<=i_n;i++)
            {
                Indicate[i]=E_wk_t[i];//自适应参考准则：E_wk变化
            }            
            Tolerent=4;
        }
        else if (ii == 2)
        {
            for (i=1;i<=i_n;i++)
            {
                Indicate[i]=f_y_t[i];//自适应参考准则：f_y变化
            }    
            Tolerent=18;
        }    
        else if (ii == 3)
        {
            for (i=1;i<=i_n;i++)
            {
                Indicate[i]=v_y_t[i];//自适应参考准则：v_y变化
            }    
            Tolerent=200;
        }    
        
        
        ///////////// 查找前Num_2个数据中的最大值
        //if (i_n==Num_1 || i_n%Num_2==0)
        //{
        //    for (i=i_n-Num_2+1;i<=i_n;i++)
        //    {
        //        if ( fabs(Indicate[i])>Tolerent)
        //        Tolerent=fabs(Indicate[i]);
        //    }
        //}
      
    
    
    
    
    
        //DEFINE_DELTAT中计数器比DEFINE_EXECUTE_AT_END多1个。前者在迭代之前执行，后者在迭代结束后执行。
        //而E_wk_t、v_x_t、v_y_t、f_x_t、f_y_t的计数器又比DEFINE_EXECUTE_AT_END少1个。
        xx1=time[i_n-1];
        xx2=time[i_n-2];
        xx3=time[i_n-3];
        y1=Indicate[i_n-1];
        y2=Indicate[i_n-2];
        y3=Indicate[i_n-3];
    
        
    
        //下面用多项式拟合预测(x6,y6)，(x7,y7)。多项式方法适合预测三角函数的一小段，小于1/4周期。
        //x6是i_n对应的时间，已知。
        //x7是i_n+1对应的时间，正式要在DEFINE_DELTAT中确定的。
        xx6=time[i_n];
        xx7=xx6+dtime[i_n];// 先用原始时间步长，确定i_n+1时刻时间
    
        
        ///////////////测试输入系数
        //fp3=fopen("test-y.txt","a");
        //fprintf(fp3,"%d\n",i_n);
        //fprintf(fp3,"%20.8f\n",xx1);
        //fprintf(fp3,"%20.8f\n",xx2);
        //fprintf(fp3,"%20.8f\n",xx3);
        //fprintf(fp3,"%20.8f\n",xx4);
        //fprintf(fp3,"%20.8f\n",xx5);
        //fprintf(fp3,"%20.8f\n",xx6);
        //fprintf(fp3,"%20.8f\n",xx7);
        ////fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",y1,y2,y3,y4,y5);
        //fclose(fp3);         
        
        
        /////时间归整化，
        /////由于时间步长太小，因此x1~x5相差不大。
        /////导致多项式拟合的系数异常大，达10^6数量级，再大的话，导致无法计算。
        /////因此，将x1~x5归一化到4、3、2、1、0。
        /////这样，求解得到的系数不会太大。
        xxxx=xx3;
        x1=(xx1-xxxx)/dtime[i_n];
        x2=(xx2-xxxx)/dtime[i_n];
        x3=(xx3-xxxx)/dtime[i_n];
        x6=(xx6-xxxx)/dtime[i_n];
        x7=(xx7-xxxx)/dtime[i_n];
    
        
        ////////////////////////测试输入系数
        //fp3=fopen("test-y.txt","a");
        //fprintf(fp3,"%d\n",i_n);
        //fprintf(fp3,"%20.8f\n",x1);
        //fprintf(fp3,"%20.8f\n",x2);
        //fprintf(fp3,"%20.8f\n",x3);
        //fprintf(fp3,"%20.8f\n",x4);
        //fprintf(fp3,"%20.8f\n",x5);
        //fprintf(fp3,"%20.8f\n",x6);
        //fprintf(fp3,"%20.8f\n",x7);
        ////fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",y1,y2,y3,y4,y5);
        //fclose(fp3);      
    
        // 用多项式拟合，要确定多项式系数a1、a2、a3、a4、a5
        // a1+a2*x+a3*x^2=y
        // [ 1, x1, x1^2, x1^3][a1]=[y1]
        // [ 1, x2, x2^2, x2^3][a2]=[y2]
        // [ 1, x3, x3^2, x3^3][a3]=[y3]
        //  c语言中数组排列如下, int a[3][4];
        //  a[0][0], a[0][1], a[0][2], a[0][3]
        //  a[1][0], a[1][1], a[1][2], a[1][3]
        //  a[2][0], a[2][1], a[2][2], a[2][3]
        //  a[3][0], a[3][1], a[3][2], a[3][3]
        //  参考 http://see.xidian.edu.cn/cpp/html/51.html
        //  因此，最终矩阵应该是这样的：
        //  [0,  0,   0,          0 ][0 ]=[0]
        //  [0,  1,  x1,  pow(x1,2) ][a1]=[y1]
        //  [0,  1,  x2,  pow(x2,2) ][a2]=[y2]
        //  [0,  1,  x3,  pow(x3,2) ][a3]=[y3]
    
    
        // 矩阵赋值
        xx[1]=x1;
        xx[2]=x2;
        xx[3]=x3;
        
        M[1][4]=y1;
        M[2][4]=y2;
        M[3][4]=y3;
    
        for (i=1;i<=3;i++)
        {
            for(j=2;j<=3;j++)
            {
            M[i][j]=pow(xx[i],j-1);
            }
        }
        
        //测试 保存原矩阵
        for (i=1;i<=3;i++)
        {
          for(j=1;j<=3;j++)
            {
            M1[i][j]=M[i][j];
            }
        }
        
        ///////////测试矩阵输入是否正确
        //fp3=fopen("test-M_0.txt","w");
        ////fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f\n",x1,x2,x3,x4,x5);
        //for (i=1;i<=5;i++)
        //{
        //    fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][1],M[i][2],M[i][3],M[i][4],M[i][5],M[i][6]);
        //}
        //fclose(fp3);    
        
        // 下三角置零
        for(j=1;j<=2;j++)
        {
            for(i=j+1;i<=3;i++)
            {  tjj  =M[j][j];
                tij  =M[i][j];
                for (k=1;k<=4;k++)
                {
                M[i][k]=M[i][k]-M[j][k]*tij/tjj;
                }
                
            }
        }
      
        //////测试下三角置零是否正确
        ////fp3=fopen("test-M_1.txt","w");
        ////for (i=1;i<=5;i++)
        ////{
        ////    fprintf(fp3,"%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",M[i][1],M[i][2],M[i][3],M[i][4],M[i][5],M[i][6]);
        ////}
        ////fclose(fp3);
        
        
        ////////////计算多项式系数
        a3=(M[3][4])                                            /M[3][3];
        a2=(M[2][4]-a3*M[2][3])                                 /M[2][2];
        a1=(M[1][4]-a3*M[1][3]-a2*M[1][2])                      /M[1][1];
        
        
        ///////////测试多项式系数是否正确
        //fp3=fopen("test-M_2.txt","a");
        //fprintf(fp3,"\n%d\n",i_n);
        //fprintf(fp3,"%15.8f\n",a1);
        //fprintf(fp3,"%15.8f\n",a2);
        //fprintf(fp3,"%15.8f\n",a3);
        //fprintf(fp3,"%15.8f%15.8f%15.8f\n",x1,a1*M1[1][1]+a2*M1[1][2]+a3*M1[1][3],y1);
        //fprintf(fp3,"%15.8f%15.8f%15.8f\n",x2,a1*M1[2][1]+a2*M1[2][2]+a3*M1[2][3],y2);
        //fprintf(fp3,"%15.8f%15.8f%15.8f\n",x3,a1*M1[3][1]+a2*M1[3][2]+a3*M1[3][3],y3);
        //fclose(fp3);    
        
        
        // 使用多项式模型，预测i_n和i_n+1时刻的功率
        y6= a1  +   a2*x6  +  a3*pow(x6,2);  
        y7= a1  +   a2*x7  +  a3*pow(x7,2);  
        
        
        ////fp3=fopen("test-work.txt","a");
        ////fprintf(fp3,"%d%20.8f%20.8f\n",i_n,xx6,y6);
        ////fclose(fp3);
        
        ////找到前300的最值，
        //for (i=i_n;i>i_n-Num_2;i--)
        //{
        // if (MaxVal<fabs(Indicate[i]))
        // MaxVal=fabs(Indicate[i]);
        //}
        
        //fp3=fopen("test-MAX.txt","a");
        //fprintf(fp3,"%d%15.8f\n",i_n,MaxVal);
        //fclose(fp3);
        //if (fabs(y7)<MaxVal/4.0)// 如果小于1/4最值，粗化时间
        //time_step=time_step*2.0;
        
        
        ////////  判定是否进行粗化时间步长
        if ( fabs(y6) < 0.5*Tolerent && fabs(y7) < 0.5*Tolerent )
        {
            time_step=dt2;  ////// 大时间步长
        }
        else
        {
            time_step=dt1; ////// 小时间步长
        }
    
    
    }
    else
        time_step=dt1; ///// 在前Num_1步，默认使用小时间步长
    

    
    
  
Label_Return:  return time_step;


} 
 

 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 //确定时间步结束的时候，耦合边界的功率。
DEFINE_EXECUTE_AT_END(execute_at_end)
{
    Domain *d;
    Thread *ttt;
    face_t f;
    FILE *fp2;//文件读写操作号。
    
    double x[ND_ND];
    double A[ND_ND];
    double A_0;
    double n_x,n_y;
    double pres;
    double v_x;
    double v_y;
    double f_x;
    double f_y;    
    double E_wk_sum=0.0;
    double f_x_sum=0.0;
    double f_y_sum=0.0;
    double v_x_sum=0.0;
    double v_y_sum=0.0;


    
    int zone_ID = 6;  
    double t_1 = CURRENT_TIME; //当前时间
    double t_0 = PREVIOUS_TIME;//上一个时间步的时间
    double dt=t_1-t_0;
    
    i_n=i_n+1; //实际运行的时间步的数量
    dtime[i_n]=dt;
    //printf("%d%18.5f%18.5f\n",i_n,dtime[i_n],dt);
    if (i_n==1)
    {
    fp2=fopen("test-x0.txt","w");
    fclose(fp2);
    }

    
    
    d = Get_Domain(1);//查找domain
    ttt = Lookup_Thread(d,zone_ID);//查找耦合边界
    
    
    //if (i_n>1) //在第n+1个时间步，x_1将被abaqus更新，而x_0则是第n个时间步的x_1的值
    //{
    //NV_V(x_0 ,=, x_1);//将当前时间步的x坐标保存
    //NV_V(y_0 ,=, y_1);//将当前时间步的y坐标保存    
    //} 
    fp2=fopen("test-x0.txt","a");
    //fprintf(fp2,"\n\n\n\n%5d\n",i_n);
    //fprintf(fp2,"%15.8f\n",t_1);
    begin_f_loop(f,ttt)//在耦合边界上遍历查找单元
    { 
      ///////// 确定单元中心坐标
      if (i_n>1) //n+1时刻，x_1要更新为存储n时刻的坐标，而x_0也更新为n-1时刻的坐标
      {
       x_0[f]=x_1[f] ;// n-1时刻，边界单元中心的坐标
       y_0[f]=y_1[f] ;// n-1时刻，边界单元中心的坐标
      }      
      F_CENTROID(x,f,ttt);//时间步结束时边界单元的中心
      x_1[f]=x[0];//n时刻，边界单元中心的x坐标
      y_1[f]=x[1];//n时刻，边界单元中心的y坐标
      if (i_n==1) //在第1个时间步，强制认为x_0和x_1是一样的。
      {
       x_0[f]=x_1[f] ;
       y_0[f]=y_1[f] ;
      }
      
      ///// 确定单元法向量
      F_AREA(A,f,ttt);  //边界单元面积向量
      n_x=A[0]/NV_MAG(A);//边界单元法向量x分量
      n_y=A[1]/NV_MAG(A);//边界单元法向量y分量
      A_0=NV_MAG(A)/1000.0;//边界单元法向量的模，即面积
                           //note:这里除以1000的原因如下。默认2D模型垂直平面方向的尺度是1，单位由求解器确定。
                           //Fluent的单位是m-kg-t，因而默认是厚度1m；
                           //Abaqus的单位是mm-T-s，默认平面应力单元的厚度为1mm。
                           //需要协调一下。
      A_0=A_0*1000.0; //这里又乘了1000，是因为功率数量级太小：4e-3 N*m/s。怕运算失真。乘以1000.
      
      /////确定单元受的压力
      if (i_n>1)
      {
      pres_0[f]=pres_1[f];// n 时刻，边界单元上的静压力
      }
      
      pres_1[f]=F_P(f,ttt); //n+1时刻，边界单元上的静压力
      
      if (i_n==1) //在第1个时间步，强制认为pres_0和pres_1是一样的。
      {
        pres_0[f]=pres_1[f];
      }
      
      
      
      //f_x=pres_0[f]*A_0*n_x+p_0*A_0*(-n_x);//n时刻，边界单元合力x分量，p_0是静载荷
      //f_y=pres_0[f]*A_0*n_y+p_0*A_0*(-n_y);//n时刻，边界单元合力y分量，p_0是静载荷
      f_x=pres_0[f]*A_0*n_x+p_0*A_0*(0.0);//n时刻，边界单元合力x分量，p_0是静载荷
      f_y=pres_0[f]*A_0*n_y+p_0*A_0*(1.0);//n时刻，边界单元合力y分量，p_0是静载荷，沿+y轴 

      //printf("%15.8f%15.8f%15.8f\n",pres,f_x,f_y);
      
      if (i_n==1)//在第1个时间步，强制认为速度为零
      {
      v_x=(x_1[f]-x_0[f])/(dtime[i_n]);
      v_y=(y_1[f]-y_0[f])/(dtime[i_n]);
      //printf("%d%15.8f%15.8f\n",i_n,dtime[i_n],v_y);
      }
      else
      {
      v_x=(x_1[f]-x_0[f])/(dtime[i_n-1]);//n时刻，边界单元中心的x方向变形速度
      v_y=(y_1[f]-y_0[f])/(dtime[i_n-1]);//n时刻，边界单元中心的y方向变形速度 
      }
      
      ///
      E_wk_sum =E_wk_sum+f_x*v_x+f_y*v_y;//n时刻，耦合边界的功率
      f_x_sum  =f_x_sum+f_x;
      f_y_sum  =f_y_sum+f_y;
      v_x_sum  =v_x_sum+v_x;
      v_y_sum  =v_y_sum+v_y;
      //if(t_1>0.0001)      
      //fprintf(fp2,"%15.8f%15.8f%15.8f%15.8f\n",x_1[f],y_1[f],v_x,v_y);
      
      //printf("%d\n",i_n);
      //if (i_n==1)
      //printf("%d%18.5f%18.5f\n",i_n,v_x_sum,v_y_sum);
    }
    end_f_loop(f,ttt)
    
    //printf("%d%18.5f%18.5f\n",i_n,dtime[i_n],dt);

    
    //fprintf(fp2,"%15.8f%15.8f\n",x_0[1],x_1[1]);
    if(i_n==1)
    {
    fprintf(fp2,"Index   dtime    time    WorkRate      f_x        f_y         v_x         v_y\n");
    }
    else
    {
    fprintf(fp2,"%d%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f%15.8f\n",i_n-1,dtime[i_n-1],t_0,E_wk_sum,f_x_sum,f_y_sum,v_x_sum,v_y_sum);
    E_wk_t[i_n-1]=E_wk_sum;
    v_x_t [i_n-1]=v_x_sum;
    v_y_t [i_n-1]=v_y_sum;
    f_x_t [i_n-1]=f_x_sum;
    f_y_t [i_n-1]=f_y_sum;
    time  [i_n-1]=t_0;
    time  [i_n]=t_1;
    }
    fclose(fp2);
};