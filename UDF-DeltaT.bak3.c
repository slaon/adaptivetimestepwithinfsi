/*********************************************************************
  UDF that changes the time step value for a time-dependent solution
 **********************************************************************/
 #include "udf.h"
 
 DEFINE_DELTAT(mydeltat,d)
 {
  face_t f;
  real A[ND_ND];
  real x[ND_ND];
  real vel[ND_ND];
  Thread *t;
  
  real time_step;
  real temp;
  real flow_time = CURRENT_TIME;
  real pres;
  real f_x,f_y;
  real n_x,n_y;
  real v_x,v_y;
  real A_0;
  real E_w;
  //real pi=3.1415926;
  //real freq=100;
  //real t_amp=5e-5;
  //real t_avg=1e-4;
  FILE *fp;
  int i,i_step;
  int zone_ID = 6;  
  //NV_S(vel,=,0);
  
  i_step=N_TIME;
  
  t = Lookup_Thread(d,zone_ID);
  
  fp=fopen("test.txt","w");
  //fprintf(fp,"%15.8f\n",flow_time);
  begin_f_loop(f,t)
  {
    F_AREA(A,f,t);
    F_CENTROID(x,f,t);
    n_x=A[0]/NV_MAG(A);//边界单元法向量x分量
    n_y=A[1]/NV_MAG(A);//边界单元法向量y分量
    pres=F_P(f,t);
    A_0=NV_MAG(A);
    f_x=pres*A_0*n_x;//边界单元受力x分量
    f_y=pres*A_0*n_y;//边界单元受力y分量
    v_x=vel[0];//边界单元的
    v_y=vel[1];//边界单元的
    //fprintf(fp,"%15.8f%15.8f%15.8f\n",A[0],A[1],A[2]);
    //fprintf(fp,"%15.8f%15.8f%15.8f\n",A[0]/NV_MAG(A),A[1]/NV_MAG(A),A[2]);
    fprintf(fp,"%3d%15.8f%15.8f%15.8f%15.8f\n",f,v_y,v_x,f_x,f_y);
    //NV_MAG(A) 为返回边界单元向量的摸
    //A[0] 面积向量在x方向分量 //A[1] 面积向量在y方向分量  //A[2] 面积向量在z方向分量
    //printf("%15f%15f%15f\n",A[0],A[1],A[2]);
  }
  end_f_loop(f,t)
  fclose(fp);
  
  //fp=fopen("test.txt","w");
  //  for(i=0;i<i_step;i++)
  //  {
  //  fscanf(fp,"%f",&temp);
  //  //printf("%d\n",i);
  //  }
  //  fscanf(fp,"%f",&time_step);
  //  printf("%d\n",i);
  //  printf("%f\n",time_step);
  //fclose(fp);
  //printf("%d\n",i_step);
  
  
  //time_step=t_avg+sin(2*pi*f*flow_time)*t_amp;
  time_step=5E-5;
  return time_step;
  

 } 
 
 
//  DEFINE_CG_MOTION(pod,dt,vel,omega,time,dtime)
// {
//  face_t f;
//  Thread *t;
//  Domain *d;          /* domain is declared as a variable  */
//  d = Get_Domain(2);   
//  int zone_ID = 6; 
//
//
//  NV_S(vel,=,0);
//  NV_S(omega,=,0);
//  t = Lookup_Thread(d,zone_ID);
//  begin_f_loop(f,t)
//  {
//  F_CENTROID(x,f,t);
//  vel[1] = sin(2.0*M_PI*1.0*x[0])*time*1.0e3;
//  }
//  end_f_loop(f,t)
// } 